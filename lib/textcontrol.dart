import 'package:flutter/material.dart';

class TextControl extends StatelessWidget{
  final String nama;
  TextControl(this.nama);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: Text('$nama',style: new TextStyle(fontSize: 35.0),),
      ),
    );
  }
}