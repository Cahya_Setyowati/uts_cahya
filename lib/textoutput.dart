import 'package:flutter/material.dart';
import './textcontrol.dart';

class TextOutput extends StatefulWidget{
  @override
  _TextOutputState createState() => _TextOutputState();
}

class _TextOutputState extends State<TextOutput>{
  String msg = 'Marhaban';

  void _changeText() {
    setState(() {
      if (msg.startsWith('Y')) {
        msg = 'Marhaban';
      }else{
        msg = 'Ya Ramdhan';
      }
      });
    }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Kami Segenap Keluarga Mengucapkan : ', style: new TextStyle(fontSize:20.0),),
          TextControl(msg),
          RaisedButton(child: Text("Next",style: new TextStyle( color: Colors.black),),color: Colors.green,onPressed:_changeText,),
        ],
      ),
    );
  }
}