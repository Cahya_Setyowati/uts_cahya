import 'package:flutter/material.dart';
import './textoutput.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'TUGAS UTS CAHYA',
      home: Scaffold(
        backgroundColor: Colors.green[100],
        appBar: AppBar(
          backgroundColor: Colors.green,
          leading: new Icon(Icons.home),
          title: Text('TUGAS UTS CAHYA'),
          actions: <Widget>[
          new Icon(Icons.search),
        ],
        ),
        body: TextOutput(),
      ),
    );
  }
}